function! SystemFirstLine(cmd)
	" Drop CR from CR-LF sequences on Windows OS
	return split(system(a:cmd), "\n")[0]
endfunction

function! AkavelListGoImports()
	" Note: http://stackoverflow.com/a/6146663/98528
	"   and :help feature-list
	if has('win32') || has('win64') || has('win16') || has('win32unix')
		let pathsep = ';'
	else
		let pathsep = ':'
	endif
	let gopath = SystemFirstLine('go env GOPATH')
	let goroot = SystemFirstLine('go env GOROOT')
	let goos = SystemFirstLine('go env GOOS')
	let goarch = SystemFirstLine('go env GOARCH')
	let goosarch = goos . '_' . goarch

	let paths = split(gopath, pathsep) + [goroot]
	let allpkgs = []
	for path in paths
		let pkgs = split(globpath(path, 'pkg/' . goosarch . '/**/*.a'), "\n")
		" FIXME(akavel): make sure path doesn't end with / or \
		let prefixlen = len(path . '/pkg/' . goosarch . '/')
		" TODO(akavel): filter out *_test.go pkgs, maybe via
		"   'wildignore'.
		" Drop path prefix (e.g. 'c:/go/pkg/windows_amd64/') and '.a' suffix
		let pkgs = map(pkgs, 'v:val[' . prefixlen . ':-3]')
		" Windows uses \ in paths, but Go needs /
		let pkgs = map(pkgs, 'substitute(v:val, "\\", "/", "g")')
		let allpkgs = allpkgs + pkgs
	endfor
	return allpkgs
endfunction

